## ⚡️️ Feature Request

- [ ] I'm using the latest version of `vscode-textmate-languageservice` available.
- [ ] I searched [existing issues][vsctmls-issues], open & closed. Yes, my feature request is new.

### Is your feature request related to a problem?
<!-- A clear & concise description of what the problem is. (e.g. The styling for semantic...). -->

### Describe the solution you'd like
<!-- A clear & concise description of what you want to happen. Add any considered drawbacks. -->

### Describe alternatives you've considered
<!-- A clear & concise description of any alternative solutions or features you've considered. -->

<!-- Checklist -->
[vsctmls-issues]: https://gitlab.com/SNDST00M/vscode-textmate-languageservice/-/issues/?sort=created_date&state=opened

/label ~feature-request
